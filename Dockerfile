FROM openjdk:11

ADD build/libs/twinsingle-bot-*.jar /twinsingle-bot.jar

CMD ["java", "-Duser.timezone=UTC", "-jar", "/twinsingle-bot.jar", "--spring.profiles.active=prod"]
