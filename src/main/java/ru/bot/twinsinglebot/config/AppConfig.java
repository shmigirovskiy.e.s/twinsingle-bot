package ru.bot.twinsinglebot.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.redis.connection.DefaultStringRedisConnection;
import org.springframework.data.redis.connection.StringRedisConnection;
import org.springframework.data.redis.core.RedisConnectionUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import static java.time.Duration.ofSeconds;

@Configuration
@EnableAsync
@EnableMongoRepositories(basePackages = "ru.bot.twinsinglebot.data.repository.mongo")
@EnableScheduling
public class AppConfig {

    @Bean
    public StringRedisConnection stringRedisConnection(StringRedisTemplate redisTemplate) {
        var factory = redisTemplate.getRequiredConnectionFactory();
        var connection = RedisConnectionUtils.getConnection(factory);
        return new DefaultStringRedisConnection(connection);
    }

    @Bean
    public RestTemplate restTemplate(ObjectMapper objectMapper) {
        var messageConverters = new RestTemplate().getMessageConverters();
        messageConverters.add(0, new MappingJackson2HttpMessageConverter(objectMapper));
        return new RestTemplateBuilder()
                .detectRequestFactory(true)
                .rootUri("http://pastebin.com/api")
                .setConnectTimeout(ofSeconds(10))
                .setReadTimeout(ofSeconds(60))
                .messageConverters(messageConverters)
                .build();
    }

}
