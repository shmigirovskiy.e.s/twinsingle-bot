package ru.bot.twinsinglebot.service.pin;

import com.google.common.hash.Hashing;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.config.TelegramBotProperties;
import ru.bot.twinsinglebot.data.pojo.pin.Destination;
import ru.bot.twinsinglebot.data.pojo.pin.castle.CastleEnemyStatus;
import ru.bot.twinsinglebot.data.pojo.pin.castle.CastlePin;
import ru.bot.twinsinglebot.data.pojo.pin.castle.CastleUserStatus;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

import java.nio.charset.StandardCharsets;
import java.util.Collections;

import static java.lang.String.format;
import static java.time.format.DateTimeFormatter.ISO_TIME;
import static java.util.Comparator.comparing;
import static java.util.Comparator.comparingInt;
import static org.apache.commons.lang3.StringUtils.joinWith;
import static org.apache.commons.lang3.StringUtils.startsWithAny;
import static ru.bot.twinsinglebot.data.pojo.pin.Destination.GENERAL_STAFF;
import static ru.bot.twinsinglebot.data.pojo.pin.UserStatus.DEAD;
import static ru.bot.twinsinglebot.data.pojo.pin.UserStatus.ON_THE_WAY;
import static ru.bot.twinsinglebot.data.pojo.pin.UserStatus.READY;

@Slf4j
@Service
public class CastlePinService extends AbstractPinService<CastlePin, CastleUserStatus> {

    public CastlePinService(TelegramBot bot, TelegramBotProperties props, UserRepository userRepository) {
        super(bot, props, userRepository);
    }

    @Override
    protected boolean checkText(String text) {
        return startsWithAny(text, "пин замки ", "пин замок ");
    }

    @Override
    protected String getText(String text) {
        return text.substring(10);
    }

    @Override
    protected void handleForward(Message message) {
        var pin = pins.get(message.getChatId());
        if (pin == null) {
            return;
        }
        if (message.getForwardDate() < pin.getStartDate() - 7200) {
            sendReply(message, "Это сообщение слишком старое");
            return;
        }
        var hash = calcHash(message);
        if (pin.getMessages().contains(hash)) {
            return;
        }
        pin.getMessages().add(hash);
        var from = message.getFrom();
        if (message.getForwardFrom().getId().equals(from.getId())) {
            if (Destination.getByValue(message.getText()) != null) {
                tryRemoveMessage(message);
            }
            return;
        }
        var userStatus = obtainUserStatus(pin, from);
        var msg = message.getText();
        if (msg.startsWith("Ты направляешься в замок")) {
            var destination = Destination.getByValue(msg.substring(25, msg.lastIndexOf('\uD83D')));
            if (destination == null) {
                log.error("handleForward destination is null; msg: {}", msg.replace('\n', ' '));
                return;
            }
            userStatus.setDestination(destination);
            userStatus.setStatus(ON_THE_WAY);
            userStatus.setArriveTime(obtainArrivalTime(message));
        } else if (msg.startsWith("Ты прибыл в замок, бой будет проходить автоматически!")) {
            if (userStatus.getDestination() == null) {
                sendMeCastle(message);
                return;
            }
            userStatus.setStatus(READY);
            userStatus.setArriveTime(obtainArrivalTime(message, 0));
        } else if (msg.startsWith("Ты направляешься в ген. штаб")) {
            userStatus.setDestination(GENERAL_STAFF);
            userStatus.setStatus(ON_THE_WAY);
            userStatus.setArriveTime(obtainArrivalTime(message));
        } else if (msg.contains("пал от рук")) {
            if (message.getForwardDate() < pin.getStartDate()) {
                sendReply(message, "Это сообщение не с этой осады!");
                return;
            }
            if (userStatus.getDestination() == null) {
                log.error("handleForward {} destination is null", userStatus.getUser().getNick());
                sendMeCastle(message);
                return;
            }
            userStatus.setStatus(DEAD);
            userStatus.setArriveTime(obtainArrivalTime(message, 600));
            userStatus.setDeaths(userStatus.getDeaths() + 1);
            var nick = obtainEnemyNick(msg);
            var enemyStatus = pin.getEnemies().computeIfAbsent(nick, CastleEnemyStatus::new);
            if (userStatus.getDestination() != GENERAL_STAFF) {
                enemyStatus.setDestination(userStatus.getDestination());
            }
            if (enemyStatus.getDeathDate() < message.getForwardDate()) {
                enemyStatus.setDeathDate(0);
            }
            enemyStatus.setKills(enemyStatus.getKills() + 1);
        } else if (msg.contains("Ты одержал победу над")) {
            if (message.getForwardDate() < pin.getStartDate()) {
                sendReply(message, "Это сообщение не с этой осады!");
                return;
            }
            if (userStatus.getDestination() == null) {
                log.error("handleForward {} destination is null", userStatus.getUser().getNick());
                sendMeCastle(message);
                return;
            }
            userStatus.setKills(userStatus.getKills() + 1);
            var nick = obtainEnemyNick(msg);
            var enemyStatus = pin.getEnemies().computeIfAbsent(nick, CastleEnemyStatus::new);
            if (userStatus.getDestination() != GENERAL_STAFF) {
                enemyStatus.setDestination(userStatus.getDestination());
            }
            if (enemyStatus.getDeathDate() < message.getForwardDate()) {
                enemyStatus.setDeathDate(message.getForwardDate());
            }
            enemyStatus.setDeaths(enemyStatus.getDeaths() + 1);
        } else if (msg.equals("Ты воскрес!")) {
            if (message.getForwardDate() < pin.getStartDate()) {
                sendReply(message, "Это сообщение не с этой осады!");
                return;
            }
            if (userStatus.getDestination() == null) {
                log.error("handleForward {} destination is null", userStatus.getUser().getNick());
                sendMeCastle(message);
                return;
            }
            userStatus.setStatus(READY);
            userStatus.setArriveTime(obtainArrivalTime(message, 0));
        } else {
            return;
        }
        pin.getStatuses().putIfAbsent(from.getId(), userStatus);
        tryRemoveMessage(message);
    }

    private String calcHash(Message message) {
        var str = String.valueOf(message.getForwardDate()) + ':' + message.getText();
        return Hashing.sha512().hashString(str, StandardCharsets.UTF_8).toString();
    }

    private String obtainEnemyNick(String msg) {
        var lines = msg.split("\\n");
        var line = lines[lines.length - 1];
        return line.substring(line.indexOf('['), line.indexOf('('));
    }

    @Override
    protected String preparePinMessage(CastlePin pin) {
        log.debug("preparePinMessage start");
        var builder = new StringBuilder();
        builder.append(pin.getHeader()).append('\n');
        var usersByDest = StreamEx.of(pin.getStatuses().values()).groupingBy(CastleUserStatus::getDestination);
        var enemiesByDest = StreamEx.of(pin.getEnemies().values()).groupingBy(CastleEnemyStatus::getDestination);
        for (var dest : Destination.values()) {
            var users = usersByDest.getOrDefault(dest, Collections.emptyList());
            log.debug("preparePinMessage dest: {}; users: {}", dest, users);
            if (!users.isEmpty()) {
                users.sort(comparing(CastleUserStatus::getArriveTime));
                builder.append('\n').append(dest.value()).append(": ").append(users.size() > 0 ? users.size() : "");
                StreamEx.of(users).map(this::getUserMention).forEach(builder::append);
                builder.append('\n');
            }

            var enemies = enemiesByDest.getOrDefault(dest, Collections.emptyList());
            log.debug("preparePinMessage dest: {}; enemies: {}", dest, enemies);
            if (!enemies.isEmpty()) {
                if (users.isEmpty()) {
                    builder.append('\n').append(dest.value()).append(":");
                }
                enemies.sort(comparingInt(CastleEnemyStatus::getKills)
                        .thenComparingInt(CastleEnemyStatus::getDeaths).reversed());
                builder.append("```\n");
                StreamEx.of(enemies).map(this::getEnemyMention).forEach(builder::append);
                builder.append("```\n");
            }
        }
        return builder.toString();
    }

    private String getUserMention(CastleUserStatus userStatus) {
        var user = userStatus.getUser();
        var time = userStatus.getArriveTime().toLocalTime().plusHours(3).format(ISO_TIME);
        if (user != null) {
            var status = userStatus.getStatus() != null ? " " + userStatus.getStatus().icon() : "";
            return format("\n[%s](tg://user?id=%d)\\[%d]%s \\[%d/%d] %s",
                    user.getNick(),
                    userStatus.getTgUserId(),
                    user.getLevel(),
                    status,
                    userStatus.getKills(),
                    userStatus.getDeaths(),
                    time);
        } else {
            var name = joinWith(" ", userStatus.getFirstName(), userStatus.getLastName()).trim();
            var status = userStatus.getStatus() != null ? " " + userStatus.getStatus().icon() : "";
            return format("\n[%s](tg://user?id=%d)%s \\[%d/%d] %s",
                    name,
                    userStatus.getTgUserId(),
                    status,
                    userStatus.getKills(),
                    userStatus.getDeaths(),
                    time);
        }
    }

    private String getEnemyMention(CastleEnemyStatus enemyStatus) {
        return format("\n%s [%d/%d]",
                enemyStatus.getNick(),
                enemyStatus.getKills(),
                enemyStatus.getDeaths()
        );
    }

    private void sendMeCastle(Message message) {
        sendReply(message, "Сначала скажи мне, в каком ты замке!");
    }

    @SneakyThrows(TelegramApiException.class)
    private void sendReply(Message message, String text) {
        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChatId());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);
        bot.execute(sendMessage);
    }

    @Scheduled(cron = "0 0 16,19 * * WED,SAT", zone = "GMT+3")
    void clearPins() {
        log.debug("clearPins start");
        pins.clear();
        log.debug("clearPins end");
    }

    @Scheduled(cron = "*/5 * * * * *")
    void checkEnemyStatus() {
        var bound = (int) (System.currentTimeMillis() / 1000) - 600;
        for (var pin : pins.values()) {
            for (var enemy : pin.getEnemies().values()) {
                if (enemy.getDeathDate() >= bound) {
                    enemy.setDestination(GENERAL_STAFF);
                }
            }
        }
    }

}
