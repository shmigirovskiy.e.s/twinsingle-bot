package ru.bot.twinsinglebot.service.pin;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatAdministrators;
import org.telegram.telegrambots.meta.api.methods.pinnedmessages.PinChatMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.ChatMember;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.component.event.NewMessageEvent;
import ru.bot.twinsinglebot.config.TelegramBotProperties;
import ru.bot.twinsinglebot.data.pojo.pin.AbstractPin;
import ru.bot.twinsinglebot.data.pojo.pin.AbstractUserStatus;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

import java.lang.reflect.ParameterizedType;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.Math.ceil;
import static java.util.Collections.emptySet;
import static ru.bot.twinsinglebot.data.pojo.pin.Destination.GENERAL_STAFF;
import static ru.bot.twinsinglebot.data.pojo.pin.UserStatus.DEAD;
import static ru.bot.twinsinglebot.data.pojo.pin.UserStatus.ON_THE_WAY;
import static ru.bot.twinsinglebot.data.pojo.pin.UserStatus.READY;

@Slf4j
@SuppressWarnings("WeakerAccess")
public abstract class AbstractPinService<P extends AbstractPin<S>, S extends AbstractUserStatus> {

    protected final Class<P> pinType;

    protected final Class<S> statusType;

    protected final Map<Long, P> pins = new ConcurrentHashMap<>();

    protected final TelegramBot bot;

    protected final TelegramBotProperties props;

    protected final UserRepository userRepository;

    protected AbstractPinService(TelegramBot bot, TelegramBotProperties props, UserRepository userRepository) {
        var superClass = getClass().getGenericSuperclass();
        //noinspection unchecked
        this.pinType = (Class<P>) ((ParameterizedType) superClass).getActualTypeArguments()[0];
        //noinspection unchecked
        this.statusType = (Class<S>) ((ParameterizedType) superClass).getActualTypeArguments()[1];
        this.bot = bot;
        this.props = props;
        this.userRepository = userRepository;
    }

    @Async
    @EventListener(NewMessageEvent.class)
    @SneakyThrows
    public void handleMessage(NewMessageEvent event) {
        log.debug("handleMessage event: {}", event.toString().replace('\n', ' '));
        var message = event.getMessage();
        if (message.getForwardFrom() != null) {
            var fwdId = message.getForwardFrom().getId();
            if (fwdId == props.getBotId() || fwdId.equals(message.getFrom().getId())) {
                handleForward(message);
            }
            return;
        }
        if (message.getPinnedMessage() != null) {
            var pinnedMessage = message.getPinnedMessage();
            var chatId = pinnedMessage.getChatId();
            var pin = pins.get(chatId);
            if (pin != null) {
                if (pin.getPinMessageId() != 0) {
                    tryRemoveMessage(chatId, pin.getPinMessageId());
                }
                pin.setPinMessageId(message.getMessageId());
            }
            return;
        }
        if (!message.hasText()) {
            return;
        }
        var text = message.getText().trim();
        if (!checkText(text.toLowerCase())) {
            return;
        }
        if (isUserChat(message) || isMine(message) || !isAdmin(message)) {
            return;
        }
        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChat().getId());
        sendMessage.setText(getText(text));

        var chatId = message.getChatId();
        var pin = pins.getOrDefault(chatId, pinType.getConstructor().newInstance());
        pin.setChatId(chatId);
        pin.setHeader(sendMessage.getText());
        if (pin.getDate() == 0) {
            pin.setDate(message.getDate());
        }

        customizeMessage(pin, message, sendMessage);

        try {
            var res = bot.execute(sendMessage);
            log.debug("handleMessage res: {}", res);

            var pinChatMessage = new PinChatMessage(chatId, res.getMessageId());
            pinChatMessage.setDisableNotification(text.contains("тест"));

            try {
                bot.execute(pinChatMessage);
            } catch (TelegramApiRequestException e) {
                log.error(e.getMessage());
                if ("Bad Request: not enough rights to pin a message".equals(e.getApiResponse())) {
                    var error = new SendMessage();
                    error.setChatId(message.getChat().getId());
                    error.setReplyToMessageId(message.getMessageId());
                    error.setText("Недостаточно прав для пина сообщения!");
                    bot.execute(error);
                }
            }

            if (pins.containsKey(chatId)) {
                tryRemoveMessage(chatId, pin.getMessageId());
                pin.setMessageId(res.getMessageId());
            } else {
                pin.setMessageId(res.getMessageId());
                pins.put(chatId, pin);
            }
        } catch (TelegramApiRequestException e) {
            log.error(e.getMessage());
            log.error(e.getApiResponse());
        }
    }

    protected abstract boolean checkText(String text);

    protected abstract String getText(String text);

    protected void customizeMessage(P pin, Message message, SendMessage sendMessage) {
    }

    protected abstract void handleForward(Message message);

    @SneakyThrows(ReflectiveOperationException.class)
    protected S obtainUserStatus(P pin, User from) {
        var fromId = from.getId();
        var userStatus = pin.getStatuses().get(fromId);
        if (userStatus == null) {
            userStatus = statusType.getConstructor().newInstance();
            userStatus.setTgUserId(fromId);
            userStatus.setFirstName(from.getFirstName());
            userStatus.setLastName(from.getLastName());
            userRepository.findById(fromId).ifPresent(userStatus::setUser);
        }
        return userStatus;
    }

    private boolean isAdmin(Message message) throws TelegramApiException {
        var chatId = message.getChatId();
        var fromId = message.getFrom().getId();
        if (props.getAdmins().computeIfAbsent(chatId, k -> emptySet()).contains(fromId)) {
            return true;
        }
        var getChatAdministrators = new GetChatAdministrators();
        getChatAdministrators.setChatId(chatId);
        var res = bot.execute(getChatAdministrators);
        return res.stream().map(ChatMember::getUser).anyMatch(e -> e.getId().equals(fromId));
    }

    private boolean isMine(Message message) {
        return props.getIdentifier().equals(message.getFrom().getUserName());
    }

    private boolean isUserChat(Message message) {
        return message.getChat().isUserChat();
    }

    protected void tryRemoveMessage(Message message) {
        tryRemoveMessage(message.getChatId(), message.getMessageId());
    }

    protected void tryRemoveMessage(long chatId, int messageId) {
        if (chatId == 0 || messageId == 0) {
            return;
        }
        var deleteMessage = new DeleteMessage(chatId, messageId);
        try {
            bot.execute(deleteMessage);
        } catch (TelegramApiException e) {
            log.error(e.getMessage());
        }
    }

    protected ZonedDateTime obtainArrivalTime(Message message) {
        var msg = message.getText();
        var arrive = msg.substring(msg.indexOf("прибудешь через"));
        var rawTime = arrive.substring(16, arrive.lastIndexOf(' '));
        log.debug("obtainArrivalTime rawTime: '{}'", rawTime);
        var time = Double.parseDouble(rawTime) * 60;
        return obtainArrivalTime(message, (int) ceil(time));
    }

    protected ZonedDateTime obtainArrivalTime(Message message, int delay) {
        return obtainArrivalTime(message.getForwardDate(), delay);
    }

    protected ZonedDateTime obtainArrivalTime(int date, int delay) {
        var ldt = LocalDateTime.ofEpochSecond(date, 0, ZoneOffset.UTC).plusSeconds(delay);
        return ZonedDateTime.of(ldt, ZoneId.systemDefault());
    }

    @SneakyThrows(TelegramApiException.class)
    protected void sendUsers(P pin) {
        log.debug("sendUsers pin: {}", pin.toString().replace('\n', ' '));
        if (pin.getStatuses().values().stream().anyMatch(e -> e.getDestination() == null)) {
            log.error("sendUsers dest is null");
            return;
        }
        var text = preparePinMessage(pin);
        if (text.equals(pin.getMessage())) {
            log.debug("sendUsers no changes");
            return;
        }
        pin.setMessage(text);

        var editMessageText = new EditMessageText();
        editMessageText.setChatId(pin.getChatId());
        editMessageText.setMessageId(pin.getMessageId());
        editMessageText.setText(text);
        editMessageText.enableMarkdown(true);

        try {
            bot.execute(editMessageText);
        } catch (TelegramApiRequestException e) {
            log.error(e.getMessage());
            log.error(e.getApiResponse());
        }
    }

    protected abstract String preparePinMessage(P pin);

    @Scheduled(cron = "*/5 * * * * *")
    protected void checkStatus() {
        var now = ZonedDateTime.now();
        for (var pin : pins.values()) {
            for (var userStatus : pin.getStatuses().values()) {
                changeStatuses(userStatus, now);
            }
            sendUsers(pin);
        }
    }

    protected void changeStatuses(S userStatus, ZonedDateTime now) {
        if (!now.isBefore(userStatus.getArriveTime())) {
            if (userStatus.getStatus() == ON_THE_WAY) {
                userStatus.setStatus(READY);
            } else if (userStatus.getStatus() == DEAD) {
                userStatus.setStatus(READY);
                userStatus.setDestination(GENERAL_STAFF);
            }
        }
    }

}
