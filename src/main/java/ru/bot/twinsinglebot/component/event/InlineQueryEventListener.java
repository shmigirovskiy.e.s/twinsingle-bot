package ru.bot.twinsinglebot.component.event;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.AnswerInlineQuery;
import org.telegram.telegrambots.meta.api.objects.inlinequery.inputmessagecontent.InputTextMessageContent;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResult;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResultArticle;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.cached.InlineQueryResultCachedSticker;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

@Slf4j
@Component
@RequiredArgsConstructor
public class InlineQueryEventListener {

    private final TelegramBot bot;

    private final UserRepository userRepository;

    private final List<InputTextMessageContent> stub = new ArrayList<>();

    private final List<InputTextMessageContent> commands = new ArrayList<>();

    {
        stub.add(new InputTextMessageContent().setMessageText("\uD83C\uDFC5 Герой"));

        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDFC5 Герой"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDF92 Рюкзак"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDC96 Пополнить здоровье"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDFDBГильд Холл"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDC3A Поиск монстров"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDCC9 Купить"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDCC8 Продать"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDCDC Мои предметы"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDFAD Создать группу"));
        commands.add(new InputTextMessageContent().setMessageText("⚖️ Проверить состав"));
        commands.add(new InputTextMessageContent().setMessageText("❌ Выйти из группы"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDFDB В ген. штаб"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDF0B Краговые шахты"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDC69\u200D\uD83D\uDE80 Терминал Basilaris"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83E\uDDDD\u200D♀ Терминал Castitas"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83E\uDD16 Терминал Aquilla"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDC231-10 Окрестности Ген. штаба"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDC25 11-20 Аванпост"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83E\uDD85 21-30 Колония Харам"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDFDC 31-40 Сеттовая пустыня"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDF40 41-50 Элан"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDFD4 Этер"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDC3AПо уровню"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDC3A Любой"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDD2A Атаковать"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDD4C Нова"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDD4C Мира"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDD4C Антарес"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDD4C Фобос"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDD4C Арэс"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDD4C Торн"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDD4C Кастор"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDD4C Конкорд"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDD4C Гром"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83D\uDD4C Алькор"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDFEF Беллатрикс"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDFEF Иерихон"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDFEF Цефея"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDFEF Супер нова"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDFF0 Альдебаран"));
        commands.add(new InputTextMessageContent().setMessageText("\uD83C\uDFF0 Бетельгейзе"));
    }

    @Async
    @EventListener(UpdateEvent.class)
    @SneakyThrows(TelegramApiException.class)
    public void onEvent(UpdateEvent event) {
        var update = event.getUpdate();
        if (!update.hasInlineQuery()) {
            return;
        }
        var inlineQuery = update.getInlineQuery();
        var from = inlineQuery.getFrom();
        var byId = userRepository.findById(from.getId());
        var answer = new AnswerInlineQuery();
        answer.setCacheTime(0);
        answer.setPersonal(true);
        answer.setInlineQueryId(inlineQuery.getId());

        if (byId.isEmpty()) {
            var content = stub.get(0);
            var stubQueryResult = new InlineQueryResultArticle();
            stubQueryResult.setId(UUID.randomUUID().toString());
            stubQueryResult.setTitle("↪️ Перешли мне свой профиль");
            stubQueryResult.setInputMessageContent(content);
            answer.setResults(List.of(stubQueryResult));
        } else {
            var user = byId.get();
            if (!"Basilaris".equals(user.getRace())) {
                var content = stub.get(0);
                var stubQueryResult = new InlineQueryResultArticle();
                stubQueryResult.setId(UUID.randomUUID().toString());
                stubQueryResult.setTitle("Эта фича только для расы \uD83D\uDC69\u200D\uD83D\uDE80Basilaris!");
                stubQueryResult.setInputMessageContent(content);
                answer.setResults(List.of(stubQueryResult));
            } else {
                var results = StreamEx
                        .of(commands)
                        .filter(e -> containsIgnoreCase(e.getMessageText(), inlineQuery.getQuery()))
                        .map(this::createQueryResult)
                        .toList();

                //бутылка для Бермана
                if (from.getId() == 124879173) {
                    var sticker = new InlineQueryResultCachedSticker();
                    sticker.setId(UUID.randomUUID().toString());
                    sticker.setStickerFileId("CAADAgADYgADURGZH_T8iEvaKbnMAg");
                    results.add(0, sticker);
                }

                answer.setCacheTime(3600);
                answer.setResults(results);
            }
        }

        bot.execute(answer);
    }

    private InlineQueryResult createQueryResult(InputTextMessageContent content) {
        var queryResult = new InlineQueryResultArticle();
        queryResult.setId(UUID.randomUUID().toString());
        queryResult.setTitle(content.getMessageText());
        queryResult.setInputMessageContent(content);
        return queryResult;
    }

}
