package ru.bot.twinsinglebot.component.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.bot.twinsinglebot.component.TelegramBot;

@Getter
public class NewMessageEvent extends ApplicationEvent {

    private final Message message;

    private final TelegramBot bot;

    public NewMessageEvent(Object source, Message message, TelegramBot bot) {
        super(source);
        this.message = message;
        this.bot = bot;
    }

}
