package ru.bot.twinsinglebot.component.event;

import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.bot.twinsinglebot.component.TelegramBot;

@Component
@AllArgsConstructor
public class NewMessageEventPublisher {

    private final ApplicationEventPublisher applicationEventPublisher;

    public void publishEvent(Message message, TelegramBot bot) {
        applicationEventPublisher.publishEvent(new NewMessageEvent(this, message, bot));
    }

}
