package ru.bot.twinsinglebot.component.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.bot.twinsinglebot.component.TelegramBot;

@Getter
public class UpdateEvent extends ApplicationEvent {

    private final Update update;

    private final TelegramBot bot;

    public UpdateEvent(Object source, Update update, TelegramBot bot) {
        super(source);
        this.update = update;
        this.bot = bot;
    }

}
