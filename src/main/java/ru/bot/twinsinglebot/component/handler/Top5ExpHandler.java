package ru.bot.twinsinglebot.component.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.data.pojo.User;
import ru.bot.twinsinglebot.data.pojo.battle.BattleResult;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;
import ru.bot.twinsinglebot.service.BattleService;

import java.math.BigDecimal;
import java.util.List;

import static java.lang.String.format;
import static java.math.RoundingMode.HALF_UP;
import static org.apache.commons.lang3.StringUtils.substring;

@Slf4j
@Component
@RequiredArgsConstructor
public class Top5ExpHandler implements CommandHandler {

    private static final String COMMAND = "/top5exp";

    private final UserRepository userRepository;

    private final BattleService battleService;

    @Override
    public void handle(Update update, TelegramBot bot) throws TelegramApiException {
        var message = update.getMessage();
        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChat().getId());
        sendMessage.setReplyToMessageId(message.getMessageId());

        var userOptional = userRepository.findById(message.getFrom().getId());
        if (userOptional.isEmpty()) {
            sendMessage.setText("Профиль не найден. Перешли в лс профиль героя и попробуй еще раз.");
        } else {
            var user = userOptional.get();
            if (!"Basilaris".equals(user.getRace())) {
                sendMessage.setText("Эта фича только для расы \uD83D\uDC69\u200D\uD83D\uDE80Basilaris!");
            } else {
                var monsterResults = battleService.topByExp(user, 5);
                sendMessage.setText(buildMessage(monsterResults, user));
                sendMessage.enableMarkdown(true);
            }
        }

        bot.execute(sendMessage);
    }

    private String buildMessage(List<BattleResult> results, User user) {
        var builder = new StringBuilder();
        builder.append("```\n");
        for (var result : results) {
            var monster = result.getUnit();
            builder.append(format("lvl:%d win:%s%% exp:%s/%s%n",
                    monster.getLevel(),
                    substring(result.getWinRate().movePointRight(2).toPlainString(), 0, 5),
                    getExp(result.getExpByWinRate(), user.getExpBonus()),
                    getExp(BigDecimal.valueOf(monster.getExp()), user.getExpBonus())
            ));
        }
        builder.append("```");
        return builder.toString();
    }

    private String getExp(BigDecimal exp, BigDecimal bonus) {
        return exp.add(exp.multiply(bonus.movePointLeft(2))).setScale(2, HALF_UP).toPlainString();
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

}
