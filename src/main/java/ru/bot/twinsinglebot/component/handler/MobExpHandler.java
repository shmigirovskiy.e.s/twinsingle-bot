package ru.bot.twinsinglebot.component.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.data.pojo.User;
import ru.bot.twinsinglebot.data.pojo.battle.BattleResult;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;
import ru.bot.twinsinglebot.service.BattleService;

import java.math.BigDecimal;

import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.math.RoundingMode.HALF_UP;
import static org.apache.commons.lang3.StringUtils.substring;

@Slf4j
@Component
@RequiredArgsConstructor
public class MobExpHandler implements CommandHandler {

    private static final String COMMAND = "/mobexp";

    private final UserRepository userRepository;

    private final BattleService battleService;

    @Override
    public void handle(Update update, TelegramBot bot) throws TelegramApiException {
        var message = update.getMessage();
        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChat().getId());
        sendMessage.setReplyToMessageId(message.getMessageId());

        var userOptional = userRepository.findById(message.getFrom().getId());
        if (userOptional.isEmpty()) {
            sendMessage.setText("Профиль не найден. Перешли в лс профиль героя и попробуй еще раз.");
        } else {
            var user = userOptional.get();
            if (!"Basilaris".equals(user.getRace())) {
                sendMessage.setText("Эта фича только для расы \uD83D\uDC69\u200D\uD83D\uDE80Basilaris!");
            } else {
                var hasError = false;
                try {
                    var monsterLevel = parseInt(message.getText().split(" ")[1]);
                    if (monsterLevel < 2 || monsterLevel > 50) {
                        hasError = true;
                    } else {
                        var result = battleService.computeResult(user, monsterLevel);
                        sendMessage.setText(buildMessage(result, user));
                    }
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
                    hasError = true;
                }
                if (hasError) {
                    sendMessage.setText("Укажите уровень моба. Пример:\n`/mobexp 30`");
                }
                sendMessage.enableMarkdown(true);
            }
        }

        bot.execute(sendMessage);
    }

    private String buildMessage(BattleResult result, User user) {
        var monster = result.getUnit();
        return format("`lvl:%d win:%s%% exp:%s/%s`",
                monster.getLevel(),
                substring(result.getWinRate().movePointRight(2).toPlainString(), 0, 5),
                getExp(result.getExpByWinRate(), user.getExpBonus()),
                getExp(BigDecimal.valueOf(monster.getExp()), user.getExpBonus())
        );
    }

    private String getExp(BigDecimal exp, BigDecimal bonus) {
        return exp.add(exp.multiply(bonus.movePointLeft(2))).setScale(0, HALF_UP).toPlainString();
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

}
