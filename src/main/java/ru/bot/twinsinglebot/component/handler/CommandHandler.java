package ru.bot.twinsinglebot.component.handler;

import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.bot.twinsinglebot.component.TelegramBot;

public interface CommandHandler {

    void handle(Update update, TelegramBot bot) throws TelegramApiException;

    String getCommand();

}
