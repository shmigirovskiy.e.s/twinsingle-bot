package ru.bot.twinsinglebot.component.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.data.pojo.TotemResult;
import ru.bot.twinsinglebot.data.repository.MonsterRepository;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;
import ru.bot.twinsinglebot.service.TotemService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static java.lang.String.format;

@Slf4j
@Component
@RequiredArgsConstructor
public class TotemsHandler implements CommandHandler {

    private static final String COMMAND = "/totems";

    private final UserRepository userRepository;

    private final MonsterRepository monsterRepository;

    private final TotemService totemService;

    @Override
    public void handle(Update update, TelegramBot bot) throws TelegramApiException {
        var message = update.getMessage();
        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChat().getId());
        sendMessage.setReplyToMessageId(message.getMessageId());

        var userOptional = userRepository.findById(message.getFrom().getId());
        if (userOptional.isEmpty()) {
            sendMessage.setText("Профиль не найден. Перешли в лс профиль героя и попробуй еще раз.");
        } else {
            var user = userOptional.get();
            if (!"Basilaris".equals(user.getRace())) {
                sendMessage.setText("Эта фича только для расы \uD83D\uDC69\u200D\uD83D\uDE80Basilaris!");
            } else {
                var split = message.getText().split(" ");
                if (split.length == 1) {
                    var totemResults = totemService.totems(user);
                    sendMessage.setText(buildMessage(totemResults));
                } else {
                    try {
                        var level = Integer.parseInt(split[1]);
                        if (level < 2 || level > 50) {
                            sendMessage.setText("Введите уровень моба (2-50)");
                            sendMessage.setText("Введите уровень моба (2-50)");
                        } else {
                            var monster = monsterRepository.getByLevel(level);
                            var totemResults = totemService.totems(user, monster);
                            sendMessage.setText(buildMessage(totemResults));
                        }
                    } catch (NumberFormatException e) {
                        sendMessage.setText("Введите уровень моба (2-50)");
                    }
                }
                sendMessage.enableMarkdown(true);
            }
        }

        bot.execute(sendMessage);
    }

    private String buildMessage(List<TotemResult> results) {
        var builder = new StringBuilder();
        builder.append("```\n");
        for (var result : results) {
            builder.append(format("%s lvl:%d cost:%d apw:%s%n",
                    result.getTotem().godName(),
                    result.getLevel(),
                    result.getCost(),
                    BigDecimal.ZERO.equals(result.getAdenPerWin())
                            ? "useless"
                            : result.getAdenPerWin().setScale(2, RoundingMode.HALF_UP).toPlainString()
            ));
        }
        builder.append("```");
        return builder.toString();
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

}
