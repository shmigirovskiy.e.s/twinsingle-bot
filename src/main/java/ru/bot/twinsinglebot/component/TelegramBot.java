package ru.bot.twinsinglebot.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.bot.twinsinglebot.component.event.NewMessageEventPublisher;
import ru.bot.twinsinglebot.component.event.UpdateEventPublisher;
import ru.bot.twinsinglebot.config.TelegramBotProperties;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

@Slf4j
@Component
public class TelegramBot extends TelegramLongPollingBot {

    private final TelegramBotProperties telegramBotProperties;

    private final UpdateEventPublisher updateEventPublisher;

    private final NewMessageEventPublisher newMessageEventPublisher;

    private final UserRepository userRepository;

    public TelegramBot(
            TelegramBotProperties telegramBotProperties,
            UpdateEventPublisher updateEventPublisher,
            NewMessageEventPublisher newMessageEventPublisher,
            UserRepository userRepository
    ) {
        super(init(telegramBotProperties));
        this.telegramBotProperties = telegramBotProperties;
        this.updateEventPublisher = updateEventPublisher;
        this.newMessageEventPublisher = newMessageEventPublisher;
        this.userRepository = userRepository;
    }

    private static DefaultBotOptions init(TelegramBotProperties props) {
        var opts = new DefaultBotOptions();
        var proxy = props.getProxy();
        if (proxy != null && proxy.getType() != null) {
            opts.setProxyType(proxy.getType());
            opts.setProxyHost(proxy.getHost());
            opts.setProxyPort(proxy.getPort());
        }
        return opts;
    }

    @Override
    public void onUpdateReceived(Update update) {
        updateEventPublisher.publishEvent(update, this);

        if (isMessage(update)) {
            var message = update.getMessage();
            newMessageEventPublisher.publishEvent(message, this);

            if (message.hasText() && message.getChat().isUserChat() && "/start".equals(message.getText())) {
                var fromId = message.getFrom().getId();
                var byId = userRepository.findById(fromId);
                byId.ifPresent(user -> {
                    if (user.isBlocked()) {
                        user.setBlocked(false);
                        userRepository.save(user);
                    }
                });
            }
        }
    }

    private boolean isMessage(Update update) {
        return update.hasMessage();
    }

    @Override
    public String getBotUsername() {
        return telegramBotProperties.getName();
    }

    @Override
    public String getBotToken() {
        return telegramBotProperties.getToken();
    }

}
