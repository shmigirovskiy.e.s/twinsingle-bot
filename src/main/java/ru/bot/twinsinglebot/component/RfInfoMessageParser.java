package ru.bot.twinsinglebot.component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import ru.bot.twinsinglebot.data.document.Message;
import ru.bot.twinsinglebot.data.pojo.cw.ChipWarUserResult;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import static java.util.Comparator.comparingInt;

@Slf4j
@Service
@RequiredArgsConstructor
public class RfInfoMessageParser {

    private final Pattern pattern = Pattern.compile("^([^(]+)\\(.*?[\\d.]+/([\\d.]+)\\)");

    private final UserRepository userRepository;

    public List<ChipWarUserResult> computeResults(List<Message> messages) {
        var resultsMap = new HashMap<Pair<String, String>, ChipWarUserResult>();

        for (var message : messages) {
            if (!message.getText().startsWith("<==Терминал")) {
                continue;
            }
            var lines = message.getText().split("\n");
            for (var line : lines) {
                if (!line.contains(" одержал победу над ")) {
                    continue;
                }
                var split = line.split(" одержал победу над ");

                var winnerMatcher = pattern.matcher(split[0]);
                if (!winnerMatcher.find()) {
                    log.error("computeResults winner pattern not found line: {}", line);
                    continue;
                }
                var winner = winnerMatcher.group(1);
                var winnerHp = winnerMatcher.group(2);

                var loserMatcher = pattern.matcher(split[1]);
                if (!loserMatcher.find()) {
                    log.error("computeResults loser pattern not found line: {}", line);
                    continue;
                }
                var loser = loserMatcher.group(1);
                var loserHp = loserMatcher.group(2);

                {
                    var result = resultsMap.computeIfAbsent(Pair.of(winner, winnerHp), k -> new ChipWarUserResult());
                    result.setNick(winner);
                    result.setWins(result.getWins() + 1);
                    if (result.getUserId() != 0) {
                        userRepository.findByNick(result.getNick()).ifPresent(user -> result.setUserId(user.getId()));
                    }
                }
                {
                    var result = resultsMap.computeIfAbsent(Pair.of(loser, loserHp), k -> new ChipWarUserResult());
                    result.setNick(loser);
                    result.setLoses(result.getLoses() + 1);
                    if (result.getUserId() != 0) {
                        userRepository.findByNick(result.getNick()).ifPresent(user -> result.setUserId(user.getId()));
                    }
                }
            }
        }

        resultsMap.forEach((s, chipWarUserResult) -> log.debug("computeResults result: {}", chipWarUserResult));

        var results = new ArrayList<>(resultsMap.values());

        results.sort(comparingInt(ChipWarUserResult::getWins)
                .reversed()
                .thenComparingInt(ChipWarUserResult::getLoses)
                .thenComparing(ChipWarUserResult::getNick)
        );

        return results;
    }

}
