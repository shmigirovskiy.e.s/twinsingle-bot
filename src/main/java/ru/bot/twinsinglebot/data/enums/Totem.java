package ru.bot.twinsinglebot.data.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Getter
@Accessors(fluent = true)
@AllArgsConstructor
public enum Totem {

    ARES("\uD83D\uDDE1Арес",
            new double[]{0, 3, 5, 7, 9, 11, 13, 16, 19, 22}),
    POSEIDON("\uD83C\uDF0AПосейдон",
            new double[]{0, 2.5, 5, 7.5, 9, 11}),
    HEPHAESTUS("\uD83D\uDCAA\uD83C\uDFFCГефест",
            new double[]{0, 2, 4, 6, 8, 10}),
    ZEUS("️⚡️Зевс",
            new double[]{0, 2, 4, 6, 8, 10}),
    CRONUS("☄️️Кронос",
            new double[]{0, 5, 8, 11, 15, 21, 26, 31, 36});

    private static final int BRONZE_COST = 2500;

    private static final int SILVER_COST = 2000;

    private static final int GOLD_COST = 3500;

    private final String godName;

    private final double[] stats;

    private final BigDecimal[] costs = new BigDecimal[]{
            BigDecimal.ZERO,
            calcCost(50000, 0, 0, 0),
            calcCost(100000, 5, 1, 0),
            calcCost(300000, 15, 5, 1),
            calcCost(500000, 30, 20, 5),
            calcCost(1000000, 70, 50, 30),
            calcCost(2000000, 120, 100, 50),
            calcCost(4000000, 250, 200, 110),
            calcCost(6000000, 340, 280, 200),
            calcCost(8000000, 500, 400, 300)
    };

    private static BigDecimal calcCost(int aden, int bronze, int silver, int gold) {
        return BigDecimal.valueOf(aden + bronze * BRONZE_COST + silver * SILVER_COST + gold * GOLD_COST);
    }

}
