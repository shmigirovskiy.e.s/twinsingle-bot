package ru.bot.twinsinglebot.data.document;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ThrowableProxyMongo {

    private String className;

    private String message;

    private ThrowableProxyMongo cause;

    private List<String> ste;

}
