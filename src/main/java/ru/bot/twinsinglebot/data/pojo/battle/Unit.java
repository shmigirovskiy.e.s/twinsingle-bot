package ru.bot.twinsinglebot.data.pojo.battle;

public interface Unit {

    int getLevel();

    double getHealth();

    double getDamage();

    double getDefense();

    double getDodge();

    double getPrecision();

    int getExp();

}
