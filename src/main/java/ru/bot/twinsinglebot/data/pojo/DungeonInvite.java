package ru.bot.twinsinglebot.data.pojo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@RequiredArgsConstructor
public class DungeonInvite {

    private final String cmd;

    private int date;

    private User leader;

    private long chatId;

    private int messageId;

    private int min;

    private int max;

    private boolean guildOnly;

}
