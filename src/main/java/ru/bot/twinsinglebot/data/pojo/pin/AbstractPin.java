package ru.bot.twinsinglebot.data.pojo.pin;

import lombok.Data;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Data
public abstract class AbstractPin<T extends AbstractUserStatus> {

    private long chatId;

    private int messageId;

    private int date;

    private int pinMessageId;

    private String header;

    private String message;

    private final Map<Integer, T> statuses = new ConcurrentHashMap<>();

}
