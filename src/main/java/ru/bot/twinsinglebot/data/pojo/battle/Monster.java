package ru.bot.twinsinglebot.data.pojo.battle;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class Monster implements Unit {

    private int level;

    private double health;

    private double damage;

    private double defense;

    private double dodge;

    private double precision;

    private int exp;

    private int gold;

}
