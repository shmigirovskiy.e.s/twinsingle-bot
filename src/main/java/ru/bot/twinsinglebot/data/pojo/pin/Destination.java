package ru.bot.twinsinglebot.data.pojo.pin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
@Accessors(fluent = true)
public enum Destination {

    BASILARIS("\uD83D\uDC69\uD83C\uDFFB\u200D\uD83D\uDE80 Basilaris"),
    CASTITAS("\uD83E\uDDDD\uD83C\uDFFB\u200D♀ ️Castitas"),
    AQUILLA("\uD83E\uDD16 Aquilla"),
    MINE("\uD83C\uDF0B Шахты"),
    NOVA("\uD83D\uDD4C Нова"),
    MIRA("\uD83D\uDD4C Мира"),
    ANTARES("\uD83D\uDD4C Антарес"),
    PHOBOS("\uD83D\uDD4C Фобос"),
    ARES("\uD83D\uDD4C Арэс"),
    TORN("\uD83D\uDD4C Торн"),
    CASTOR("\uD83D\uDD4C Кастор"),
    CONCORDE("\uD83D\uDD4C Конкорд"),
    GROM("\uD83D\uDD4C Гром"),
    ALCOR("\uD83D\uDD4C Алькор"),
    BELLATRIX("\uD83C\uDFEF Беллатрикс"),
    JERICHO("\uD83C\uDFEF Иерихон"),
    CEPHEI("\uD83C\uDFEF Цефея"),
    SUPERNOVA("\uD83C\uDFEF Супер нова"),
    ALDEBARAN("\uD83C\uDFF0 Альдебаран"),
    BETELGEUSE("\uD83C\uDFF0 Бетельгейзе"),
    GENERAL_STAFF("\uD83C\uDFDB Ген. штаб");

    private String value;

    private static final Map<String, Destination> valueMap;

    static {
        var map = new HashMap<String, Destination>();
        for (var dest : values()) {
            map.put(dest.value(), dest);
        }
        valueMap = Map.copyOf(map);
    }

    public static Destination getByValue(String value) {
        return valueMap.get(value);
    }

}
