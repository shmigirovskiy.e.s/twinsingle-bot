package ru.bot.twinsinglebot.data.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.bot.twinsinglebot.data.document.Message;

import java.util.List;

public interface MessageRepository extends MongoRepository<Message, Long> {

    Message findFirstByTextOrderByIdDesc(String text);

    Message findFirstByTextAndDateAfterOrderByIdDesc(String text, int date);

    List<Message> findByDateBetween(int start, int end);

}
