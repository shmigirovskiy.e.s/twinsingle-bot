package ru.bot.twinsinglebot.data.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.bot.twinsinglebot.data.pojo.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends MongoRepository<User, Integer> {

    Optional<User> findByNick(String nick);

    List<User> findAllByLevelBetweenAndRace(int min, int max, String race);

    List<User> findAllByLevelBetweenAndGuild(int min, int max, String guild);

}
