package ru.bot.twinsinglebot.data.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.bot.twinsinglebot.data.pojo.cw.ChipWarResult;

public interface ChipWarResultRepository extends MongoRepository<ChipWarResult, Integer> {

    ChipWarResult findFirstByOrderByDateDesc();

    ChipWarResult findByDate(int date);

}
